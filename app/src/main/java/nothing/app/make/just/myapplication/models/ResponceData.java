
package nothing.app.make.just.myapplication.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;


public class ResponceData extends RealmObject {


    @SerializedName("response")
    @Expose
    private Responsee response;

    public Responsee getResponse() {
        return response;
    }

    public void setResponse(Responsee response) {
        this.response = response;
    }

}
