package nothing.app.make.just.myapplication;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import java.util.concurrent.TimeUnit;

import nothing.app.make.just.myapplication.models.ResponceData;
//import nothing.app.make.just.myapplication.models.ResponseData;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class NetworkService {

    public static NetworkService instance;

    private static final String BASE_URL = "https://content.guardianapis.com";

    private static Gson gson = new GsonBuilder().setLenient().create();
    private static OkHttpClient  client = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build();

    Api api;
    private NetworkService(Api api) {

        this.api = api;

    }


    private NetworkService() {
    }


    public static NetworkService getInstance() {

        if (instance == null) {

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();


            instance = new NetworkService(retrofit.create(Api.class));

        }

        return instance;

    }


    public void getAllusers(final GetAllUsers getAllUsers) {
//        api.getAllUsers("ef5d03c5-0e3a-40da-b816-02d926a26e35","thumbnail").enqueue(new Callback<JsonElement>() {
//            @Override
//            public void onResponse(Call<JsonElement> call, Responsee<JsonElement> response) {
//                if (response.isSuccessful() && response.body() != null && !response.body().isJsonNull()){
//                    Log.e("vach json = " , new Gson().toJson(response.body()));
//                    ResponceData responseData = new Gson().fromJson(response.body(),ResponceData.class);
//                    getAllUsers.onSuccess(responseData);
//                }else {
//                    getAllUsers.onFailure("");
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonElement> call, Throwable t) {
//                getAllUsers.onFailure(t.getMessage());
//            }
//        });
        api.getAllUsers("ef5d03c5-0e3a-40da-b816-02d926a26e35","thumbnail").enqueue(new Callback<ResponceData>() {
            @Override
            public void onResponse(Call<ResponceData> call, Response<ResponceData> response) {
                if (response.isSuccessful() && response.body() != null) {
                    getAllUsers.onSuccess(response.body());
                }else {
                    getAllUsers.onFailure("dsadasdasda");
                }
            }

            @Override
            public void onFailure(Call<ResponceData> call, Throwable t) {
                    getAllUsers.onFailure("Dsadadaaaad");
            }
        });
    }



}