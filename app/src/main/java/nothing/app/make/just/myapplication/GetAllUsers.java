package nothing.app.make.just.myapplication;

import nothing.app.make.just.myapplication.models.ResponceData;

public interface GetAllUsers {

    void onSuccess(ResponceData results);
    void onFailure(String message);

}
