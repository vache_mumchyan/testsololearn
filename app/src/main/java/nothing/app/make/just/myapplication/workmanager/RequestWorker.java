package nothing.app.make.just.myapplication.workmanager;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.pixplicity.easyprefs.library.Prefs;

import androidx.work.Worker;
import androidx.work.WorkerParameters;
import nothing.app.make.just.myapplication.GetAllUsers;
import nothing.app.make.just.myapplication.MainActivity;
import nothing.app.make.just.myapplication.NetworkService;
import nothing.app.make.just.myapplication.R;
import nothing.app.make.just.myapplication.models.ResponceData;

import static nothing.app.make.just.myapplication.TestApplication.CHANNEL_ID;

public class RequestWorker extends Worker {


    public RequestWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
        Log.i("++++++", "Start RequestWorker");
        // Ay stex Retrofit requst ara bayc execute metodov vorovhetev sinxron a ashxatum workmnager@ chi kara callback spasi


        NetworkService.getInstance().getAllusers(new GetAllUsers() {
            @Override
            public void onSuccess(ResponceData results) {
                if (Prefs.getString("total", "").equals(String.valueOf(results.getResponse().getTotal()))) {
                    System.out.println("++++++havasar en" +results.getResponse().getTotal());
                }else {
                    System.out.println("++++update anela petk" +results.getResponse().getTotal());
                    Prefs.putString("total", String.valueOf(results.getResponse().getTotal()));

                    sendNotification("Yes","Message is coming");
                }
            }

            @Override
            public void onFailure(String message) {
                System.out.println(message);
            }
        });
        WorkManagerHelper.run();
        return Result.success();
    }

    public void sendNotification(String title, String message) {
        NotificationManager notificationManager = (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        //If on Oreo then notification required a notification channel.
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, "Example channel", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(notificationChannel);
        }
        Intent showIntent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0, showIntent, 0);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(getApplicationContext(), CHANNEL_ID)
                .setContentTitle(title)
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setContentText(message)
                .setSmallIcon(R.mipmap.ic_launcher);

        notification.build().flags |= Notification.FLAG_AUTO_CANCEL;


        notificationManager.notify(7, notification.build());
    }
}
