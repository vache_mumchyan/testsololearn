package nothing.app.make.just.myapplication;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

import nothing.app.make.just.myapplication.models.ResponceData;
//import nothing.app.make.just.myapplication.models.ResponseData;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface Api {

//    @GET("search")
//    Call<Responsee> getAllUsers(@Query("api-key") String apiKey);

    @GET("search")
    Call<ResponceData> getAllUsers(@Query("api-key") String apiKey, @Query("show-fields") String thumb);


//    @GET("search")
//    Call<ResponseData.Responsee> getNewItem(@Query("api-key") String apiKey);
}
