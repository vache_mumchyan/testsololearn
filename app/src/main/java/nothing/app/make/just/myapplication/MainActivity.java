package nothing.app.make.just.myapplication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.pixplicity.easyprefs.library.Prefs;

import java.util.ArrayList;
import java.util.List;

import nothing.app.make.just.myapplication.models.Fields;
import nothing.app.make.just.myapplication.models.ResponceData;
import nothing.app.make.just.myapplication.models.Result;
import nothing.app.make.just.myapplication.models.TestModel;
import nothing.app.make.just.myapplication.workmanager.WorkManagerHelper;

//import nothing.app.make.just.myapplication.models.ResponseData;

public class MainActivity extends AppCompatActivity implements NewsAdapter.ImageCliclListener {


    private RecyclerView recyclerView;
    private NewsAdapter newsAdapter;
    private ProgressBar progressBar;
    private RealmProvider realmProvider;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realmProvider = new RealmProvider(this);
        realmProvider.init();


        recyclerView = findViewById(R.id.recyclerView);
        progressBar = findViewById(R.id.progressBar);
        newsAdapter = new NewsAdapter(this);
        newsAdapter.setCliclListener(this);


        final SpannedGridLayoutManager gridLayoutManager = new SpannedGridLayoutManager(new SpannedGridLayoutManager.GridSpanLookup() {
            @Override
            public SpannedGridLayoutManager.SpanInfo getSpanInfo(int position) {
                SpannedGridLayoutManager.SpanInfo spanInfo = new SpannedGridLayoutManager.SpanInfo(1, 1);
                if (position % 6 == 0 || position % 6 == 4) {
                    return new SpannedGridLayoutManager.SpanInfo(2, 2);
                } else {
                    return new SpannedGridLayoutManager.SpanInfo(1, 1);
                }
                //return spanInfo;
            }
        }, 3, 1f);
        final RecyclerView.LayoutManager lm = new LinearLayoutManager(
                this, LinearLayoutManager.VERTICAL, false);


        if (isNetworkConnected()) {
            NetworkService.getInstance().getAllusers(new GetAllUsers() {
                @Override
                public void onSuccess(ResponceData results) {
                    Prefs.putString("total", String.valueOf(results.getResponse().getTotal()));
                    progressBar.setVisibility(View.GONE);
                    System.out.println(results.getResponse());
                    recyclerView.setLayoutManager(gridLayoutManager);
                    newsAdapter.addAll(results.getResponse().getResults());
                    recyclerView.setAdapter(newsAdapter);
                }
                @Override
                public void onFailure(String message) {
                    System.out.println(message);
                }
            });
        } else {
            List<TestModel> realmList = new ArrayList<>(realmProvider.getAllNews());
            List<Result> newList = new ArrayList<>();
            for (TestModel testModel : realmList) {
                Result item = new Result();
                Fields fields = new Fields();
                fields.setThumbnail(testModel.getImageUrl());
                item.setFields(fields);
                item.setSectionName(testModel.getSectionName());
                item.setWebTitle(testModel.getWebTitle());
                newList.add(item);
            }
            newsAdapter.addAll(newList);
            recyclerView.setAdapter(newsAdapter);
            newsAdapter.notifyDataSetChanged();
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        WorkManagerHelper.cancel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        WorkManagerHelper.run();

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null;
    }


    @Override
    public void onSelectListener(Result result) {
        realmProvider.put(result);
    }
}
