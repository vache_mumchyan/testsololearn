package nothing.app.make.just.myapplication;

import android.content.Context;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import nothing.app.make.just.myapplication.models.ResponceData;
import nothing.app.make.just.myapplication.models.Result;
import nothing.app.make.just.myapplication.models.TestModel;
//import nothing.app.make.just.myapplication.models.ResponseData;

public class RealmProvider {

    private Realm realm;
    private Context context;

    public RealmProvider(Context context) {
        this.context = context;
    }
    public void init(){
        Realm.init(context);
        realm = Realm.getDefaultInstance();
    }

    public void put(Result result){
        realm.beginTransaction();
        TestModel item = realm.createObject(TestModel.class);
        item.setSectionName(result.getSectionName());
        item.setWebTitle(result.getWebTitle());
        item.setImageUrl(result.getFields().getThumbnail());

        realm.commitTransaction();
    }

    public List<TestModel> getAllNews(){
        RealmResults<TestModel> realmResults = realm.where(TestModel.class).findAll();
        return realmResults.subList(0,realmResults.size());
    }
}
