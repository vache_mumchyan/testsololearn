package nothing.app.make.just.myapplication;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.pixplicity.easyprefs.library.Prefs;

public class DetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        final String section = intent.getStringExtra("keySection");
        final String webTitle = intent.getStringExtra("keyName");
        final String image = intent.getStringExtra("keyImage");
        final TextView title = findViewById(R.id.tv_detail_title);
        final TextView overview = findViewById(R.id.tv_detail_overview);
        final ImageView poster = findViewById(R.id.img_detail);

        title.setText(section);
        overview.setText(webTitle);

        Glide.with(DetailActivity.this)
                .load(image)
                .into(poster);

        System.out.println(""+Prefs.getString("total",""));
    }
}
