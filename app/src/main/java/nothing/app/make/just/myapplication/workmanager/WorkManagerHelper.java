package nothing.app.make.just.myapplication.workmanager;

import java.util.concurrent.TimeUnit;

import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

public class WorkManagerHelper {

    private static final String NAME_REQUEST_WORKER = "request_worker";

    public static void run() {
        System.out.println("+++example");
        OneTimeWorkRequest workRequest = new OneTimeWorkRequest.Builder(RequestWorker.class)
                .setInitialDelay(30, TimeUnit.SECONDS)
                .build();
        WorkManager.getInstance().enqueueUniqueWork(NAME_REQUEST_WORKER
                , ExistingWorkPolicy.REPLACE
                , workRequest);
    }

    public static void cancel() {
        WorkManager.getInstance().cancelUniqueWork(NAME_REQUEST_WORKER);
    }
}
