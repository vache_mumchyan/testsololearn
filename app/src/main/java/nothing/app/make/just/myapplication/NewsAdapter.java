package nothing.app.make.just.myapplication;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import nothing.app.make.just.myapplication.models.Result;
import nothing.app.make.just.myapplication.models.TestModel;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.MovieViewHolder> {

    List<Result> movieList;
    Context context;
    ImageCliclListener cliclListener;


    public void setCliclListener(ImageCliclListener cliclListener) {
        this.cliclListener = cliclListener;
    }

    public NewsAdapter(Context context) {
        this.context = context;
        movieList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);
        return new MovieViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, final int position) {
        final Result currentMovie = movieList.get(position);
        holder.textView.setText(currentMovie.getSectionName());
        Glide.with(context)
                .load(currentMovie.getFields().getThumbnail())
                .into(holder.imageView);

        holder.imgSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    cliclListener.onSelectListener(currentMovie);
            }
        });

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MovieViewHolder extends RecyclerView.ViewHolder {

        private ImageView imageView;
        private ImageView imgSave;
        private TextView textView;

        public MovieViewHolder(final View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.tv_title);
            imageView = itemView.findViewById(R.id.imageView);
            imgSave = itemView.findViewById(R.id.img_save);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Result currentMovie = movieList.get(getAdapterPosition());
                    Intent intent = new Intent(context, DetailActivity.class);
                    intent.putExtra("keySection", currentMovie.getSectionName());
                    intent.putExtra("keyName", currentMovie.getWebTitle());
                    intent.putExtra("keyImage", currentMovie.getFields().getThumbnail());
                    context.startActivity(intent);
                }
            });
        }
    }

    public interface ImageCliclListener {
        void onSelectListener(Result name);
    }

    public void addAll(List<Result> list) {
        movieList.clear();
        if (list.size() != 0) {
            movieList.addAll(list);
        }
    }

//    public void addAllFromDB(List<TestModel> list){
//        movieList.clear();
//        if (list.size() != 0){
//            movieList.addAll(list);
//        }
//    }
}
